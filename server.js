const express = require('express');
const next = require('next');
const LeadChain = require('./blockchain/index');

const port = parseInt(process.env.PORT, 10) || 3000;
const dev = process.env.NODE_ENV !== 'production';
const app = next({ dev });
const handle = app.getRequestHandler();

const leadChain = LeadChain();
let db = { kickbackCount: 1, kickAmount: 1, merchantRevenue: 11 };

app.prepare().then(() => {
  const server = express();

  server.get('/api/influencer/:id/stats', (req, res) => {
    return leadChain.fetchLastBlock().then(resp => {
      console.log(resp);
      return res.json({
        user: req.params.id,
        ...db,
      });
    });
  });

  server.get('/api/influencer/:id/conversion', (req, res) => {
    return leadChain.createConversion(`${req.query.amount}`).then(resp => {
      console.log(resp);
      db = {
        kickbackCount: db.kickbackCount + 1,
        kickAmount: db.kickAmount + 1,
        merchantRevenue: +db.merchantRevenue + +req.query.amount,
      };

      return res.json({ user: req.params.id, ...db });
    });
  });

  server.get('*', (req, res) => {
    return handle(req, res);
  });

  server.listen(port, err => {
    if (err) throw err;
    console.log(`> Ready on http://localhost:${port}`);
  });
});
