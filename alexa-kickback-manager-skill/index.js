const Alexa = require('alexa-sdk');

exports.handler = (event, context) => {
  const alexa = Alexa.handler(event, context);
  alexa.registerHandlers(handlers);
  alexa.execute();
};

const handlers = {
  'LaunchRequest': function() {
    const prompt = `Take a break manager, it's beer time!`;
    this.emit(':ask', prompt, prompt);
  },
  'Stats': function() {
    this.emit(':tell', `here are your stats, it's beer time!`);
  }

}
