const blockchain = require('mastercard-blockchain');
const encoding = 'base64';
const fs = require('fs');
const path = require('path');
const protobuf = require('protobufjs');
const consumerKey = require('../config/consumer-key');

const LeadChain = function LeadChain() {
  const keystorePath = '../config/team15.p12';
  const keyAlias = 'keyalias';
  const storePass = 'keystorepassword';
  let appID = null;
  let msgClassDef = null;

  const authentication = new blockchain.MasterCardAPI.OAuth(
    consumerKey.key,
    keystorePath,
    keyAlias,
    storePass,
  );

  blockchain.MasterCardAPI.init({
    sandbox: true,
    debug: false,
    authentication,
  });

  protobuf.load('./blockchain/message.proto', (err, root) => {
    if (err) {
      console.log('error', err);
    } else {
      const nested = guessNested(root);
      if (nested && 2 == nested.length) {
        appID = nested[0];
        msgClassDef = root.lookupType(appID + '.' + nested[1]);
        console.log('initialized appID', appID, 'nested[1]', nested[1]);
      } else {
        console.log('could not read message class def from', protoFile);
      }
    }
  });

  return {
    createConversion(text) {
      // TODO text amount
      const payload = { text };
      const errMsg = msgClassDef.verify(payload);
      console.log('payload', payload);
      console.log('pre-errMsg', errMsg);

      return new Promise((resolve, reject) => {
        if (errMsg) {
          console.log('errMsg', errMsg);
          reject(errMsg);
        } else {
          const message = msgClassDef.create(payload);
          blockchain.TransactionEntry.create(
            {
              app: appID,
              encoding: encoding,
              value: msgClassDef
                .encode(message)
                .finish()
                .toString(encoding),
            },
            (err, createResp) => {
              if (err) {
                console.log('createResp err', err);
                reject(err);
              }
              console.log('createResp', createResp);
              resolve(createResp);
            },
          );
        }
      });
    },
    fetchLastBlock() {
      // retrieveLastConfirmedBlock
      return new Promise((resolve, reject) => {
        blockchain.Block.list({}, (err, info) => {
          console.log('fetchLastBlock', info);
          resolve(info);
        });
      });
    },
  };
};

function getProperties(obj) {
  var ret = [];
  for (var name in obj) {
    if (obj.hasOwnProperty(name)) {
      ret.push(name);
    }
  }
  return ret;
}

function guessNested(root) {
  var props = getProperties(root.nested);
  var firstChild = getProperties(root.nested[props[0]].nested);
  return [props[0], firstChild[0]];
}

module.exports = LeadChain;
