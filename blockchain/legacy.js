const blockchain = require('mastercard-blockchain');
const consumerKey = require('../config/consumer-key');
const encoding = 'base64';
const fs = require('fs');
const protobuf = require('protobufjs');

// storePass: {
//   description: 'keystore password (mastercard developers)',
//   default: 'keystorepassword',
// },
// keyAlias: {
//   description: 'key alias (mastercard developers)',
//   default: 'keyalias',
// },
// protoFile: {
//   description: 'the path to the protobuf File',
//   default: 'message.proto',

let protoFile = null;
let appID = null;
let msgClassDef = null;
const nested = null;

// updateNode
// name: 'newProtoFile', // description: 'Protofile',
// default: 'message.proto', fs.existsSync(value);
const callback = function callback() {};
const data = {};
protoFile = data.newProtoFile;
protobuf.load(protoFile, callback);
appID = nested[0];
msgClassDef = root.lookupType(`${appID}.${nested[1]}`);
blockchain.App.update(
  {
    id: appID,
    name: appID,
    description: '',
    version: 0,
    definition: {
      format: 'proto3',
      encoding,
      messages: fs.readFileSync(protoFile).toString(encoding),
    },
  },
  () => {}, // callback,
);
blockchain.App.read(appID, {}, callback);

// createEntry
const payload = { text: data.textEntry };
// const errMsg = msgClassDef.verify(payload);
const message = msgClassDef.create(payload);
blockchain.TransactionEntry.create(
  {
    app: appID,
    encoding,
    value: msgClassDef
      .encode(message)
      .finish()
      .toString(encoding),
  },
  callback,
);

// retrieveEntry
const ctx = {};
blockchain.TransactionEntry.read('', { hash: data.hash }, callback);
ctx.data = data;
// const message = msgClassDef.decode(new Buffer(data.value, 'hex'));
const object = msgClassDef.toObject(message, {
  longs: String,
  enums: String,
  bytes: String,
});

// retrieveBlock
blockchain.Block.read(data.blockId, {}, callback);

// retrieveLastConfirmedBlock
blockchain.Block.list({}, callback);

// 'the path to your keystore (mastercard developers)',
const keystorePath = fs.existsSync(__dirname, '../config/team15.p12');
const keyAlias = null;
const storePass = null;

const authentication = new blockchain.MasterCardAPI.OAuth(
  consumerKey,
  keystorePath,
  keyAlias,
  storePass,
);

blockchain.MasterCardAPI.init({
  sandbox: true,
  debug: '-v',
  authentication,
});

protoFile = result.protoFile;
protobuf.load(protoFile, callback);

appID = nested[0];
msgClassDef = root.lookupType(`${appID}.${nested[1]}`);
