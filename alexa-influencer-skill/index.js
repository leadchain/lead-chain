const Alexa = require("alexa-sdk");
require('isomorphic-fetch');

exports.handler = (event, context) => {
  const alexa = Alexa.handler(event, context);
  alexa.registerHandlers(handlers);
  alexa.execute();
};

const handlers = {
  LaunchRequest: function() {
    this.emit("Stats");
  },
  Stats: function() {
    fetch("https://kickback.cuadranteweb.com/api/influencer/1/stats")
      .then(response => {
        return response.json();
      }).then(json => {
        console.log(json);
        const kickbackCount = json.kickbackCount;
        const kickAmount = json.kickAmount;
        const merchantRevenue = json.merchantRevenue;
        this.emit(
          ":tell",
          `You have referred ${kickbackCount} people, which has converted ${merchantRevenue} in revenue, earning you $${kickAmount} as kick back. <say-as interpret-as="interjection">cha ching!</say-as>`
        );
      })
      .catch(error => {
        console.log(error);
        //fallback for demo
        this.emit(":tell", `You have referred 10 people, which has converted $138.93 in revenue, earning you $11 as kick back. <say-as interpret-as="interjection">ker ching!</say-as>`);
      });
  }
};
