import React, { Component } from 'react';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import injectTapEventPlugin from 'react-tap-event-plugin';
import Head from 'next/head';
import {
  Card,
  CardActions,
  CardHeader,
  CardMedia,
  CardTitle,
  CardText,
} from 'material-ui/Card';
import SuccessIcon from 'material-ui/svg-icons/action/check-circle';
import { green700 } from 'material-ui/styles/colors';

// Make sure react-tap-event-plugin only gets injected once
// Needed for material-ui
if (!process.tapEventInjected) {
  injectTapEventPlugin();
  process.tapEventInjected = true;
}

class Index extends Component {
  static getInitialProps({ req }) {
    // Ensures material-ui renders the correct css prefixes server-side
    const userAgent = process.browser
      ? navigator.userAgent
      : req.headers['user-agent'];

    return { userAgent };
  }

  constructor(props, context) {
    super(props, context);

    this.state = {
      success: false,
    };
  }
  purchase = () => {
    fetch('/api/influencer/1/conversion?amount=11').then(() => {
      this.setState({ success: true });
    });
  };

  render() {
    const props = this.props;
    return (
      <MuiThemeProvider muiTheme={getMuiTheme({ userAgent: props.userAgent })}>
        <div>
          <Head>
            <link
              href="https://fonts.googleapis.com/css?family=Poppins"
              rel="stylesheet"
            />
            <meta
              name="viewport"
              content="width=device-width, initial-scale=1"
            />
          </Head>
          <Card>
            <CardHeader
              title="orlyzomg"
              subtitle="City of Fullerton"
              avatar="https://scontent-lax3-1.cdninstagram.com/t51.2885-19/11296729_1486605864989680_745755913_a.jpg"
            />
            <CardMedia>
              <img src="/static/offer.png" alt="" />
            </CardMedia>
            <CardTitle title="CRITICAL BAND" subtitle="Juicy Tropical" />
            <CardText>
              This deeply juicy stunner in the mold of City of the Sun and
              Booming Rollers will be replacing Aurora in the seasonal line-up
              next year. While our beloved red rye IPA will undoubtedly be
              missed, we think you’ll agree with our decision once you get a
              face-full of this outrageously tasty IPA. Brewed with Denali,
              Ekuanot, Citra, and Centennial hops, Critical Band is a blast wave
              of pineapple, papaya, and pink grapefruit over a crisp, restrained
              malt bill, before wrapping up in a soft, round finish that leaves
              you with warm feelings and an intense desire for another sip.
            </CardText>
            <CardActions>
              <div className="checkout">
                <h2>Modern Times Critical Band IPA 16 oz Can Four Pack</h2>
                <h1>$11.00</h1>
                {this.state.success ? (
                  <div className="success">
                    <SuccessIcon
                      color={green700}
                      style={{
                        width: '100px',
                        height: '100px',
                      }}
                    />
                  </div>
                ) : (
                  <div className="buy" onClick={this.purchase}>
                    <img src="/static/pay.png" alt="" className="applePay" />
                  </div>
                )}
              </div>
            </CardActions>
          </Card>
          <style jsx>{`
            .checkout {
              text-align: center;
              margin-bottom: 3rem;
            }
            .buy {
            }
            .applePay {
              width: 125px;
            }
            .success {
              text-align: center;
              margin-bottom: 3rem;
            }
          `}</style>

          <style jsx global>{`
            body {
              font-family: 'Poppins', sans-serif;
            }
          `}</style>
        </div>
      </MuiThemeProvider>
    );
  }
}

export default Index;
